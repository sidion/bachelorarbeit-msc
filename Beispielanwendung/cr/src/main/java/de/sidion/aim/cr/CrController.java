package de.sidion.aim.cr;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.sidion.aim.rbpmlogging.ProcessState;
import de.sidion.aim.rbpmlogging.RBPMLogger;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/contractrequest")
public class CrController {
    // docker host defined in docker-compose.yml
    private static final String CRM_URL = "http://crm-app:8081/person/";
    private static final String CMS_URL = "http://cms-app:8082/contract/";

    private static int COUNTER = 0;

    // localhosts
    //private static final String CRM_URL = "http://localhost:8081/person/";
    //private static final String CMS_URL = "http://localhost:8082/contract/";

    private final ObjectMapper mapper = new ObjectMapper();

    private final Map<Long, ContractRequest> contractRequestCollection = new HashMap<>();


    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<?> create(@RequestBody ContractRequest contractRequest) {
        RBPMLogger rbpmLogger = new RBPMLogger("Vertragsanfrage erstellen", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        rbpmLogger.log(ProcessState.STARTED);

        if (contractRequest != null) {
            Person salesman;
            Person customer;
            if (COUNTER % 2 == 0) {
                rbpmLogger.log(ProcessState.WORK_IN_PROGRESS, "Kunden erstellen");
                customer = createPerson(contractRequest.getCustomerFirstName(), contractRequest.getCustomerLastName(), contractRequest.getCustomerCompany());
                contractRequest.setCustomerUrl(CRM_URL + customer.getId());

                rbpmLogger.log(ProcessState.WORK_IN_PROGRESS, "Verkäufer erstellen");
                salesman = createPerson(contractRequest.getSalesmenFirstName(), contractRequest.getSalesmenLastName(), contractRequest.getSalesmanCompany());
                contractRequest.setSalesmenUrl(CRM_URL + salesman.getId());
            } else {
                rbpmLogger.log(ProcessState.WORK_IN_PROGRESS, "Verkäufer erstellen");
                salesman = createPerson(contractRequest.getSalesmenFirstName(), contractRequest.getSalesmenLastName(), contractRequest.getSalesmanCompany());
                contractRequest.setSalesmenUrl(CRM_URL + salesman.getId());

                rbpmLogger.log(ProcessState.WORK_IN_PROGRESS, "Kunden erstellen");
                customer = createPerson(contractRequest.getCustomerFirstName(), contractRequest.getCustomerLastName(), contractRequest.getCustomerCompany());
                contractRequest.setCustomerUrl(CRM_URL + customer.getId());
            }
            COUNTER++;

            rbpmLogger.log(ProcessState.WORK_IN_PROGRESS, "Vertrag erstellen");
            Contract contract = createContract(salesman.getId(), customer.getId(), contractRequest.getProductName());
            contractRequest.setContractUrl(CMS_URL + contract.getId());

            rbpmLogger.log(ProcessState.WORK_IN_PROGRESS, "Vertragsanfrage speichern");
            Long id = (long) contractRequestCollection.size();
            contractRequest.setId(id);
            contractRequestCollection.put(contractRequest.getId(), contractRequest);
            rbpmLogger.log(ProcessState.SUCCESSFULLY_DONE, "Vertragserstellung erfolgreich abgeschlossen");
        } else {
            rbpmLogger.log(ProcessState.UNSUCCESSFULLY_DONE, "Kein Vertrag erstellt");
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(contractRequest);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<?> getAll() {
        RBPMLogger logger = new RBPMLogger("Alle Vertragsanfragen anzeigen", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        logger.log(ProcessState.STARTED);
        logger.log(ProcessState.WORK_IN_PROGRESS, "Vertragsanfragen laden");
        logger.log(ProcessState.SUCCESSFULLY_DONE, "Vertragsanfragen zurückgeben");
        return ResponseEntity.status(HttpStatus.OK).body(contractRequestCollection.values());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> get(
            @ApiParam(required = true, name = "id", value = "ID of the contract request you want to get")
            @PathVariable Long id) {
        RBPMLogger logger = new RBPMLogger("Eine Vertragsanfrage anzeigen", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        logger.log(ProcessState.STARTED);
        logger.log(ProcessState.WORK_IN_PROGRESS, "Passenden Vertragsanfrage suchen");
        if (contractRequestCollection.containsKey(id)) {
            logger.log(ProcessState.SUCCESSFULLY_DONE, "Vertragsanfrage zurückgeben");
            return ResponseEntity.status(HttpStatus.OK).body(contractRequestCollection.get(id));
        } else {
            logger.log(ProcessState.UNSUCCESSFULLY_DONE, "Vertragsanfrage nicht gefunden");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("There is no entry with corresponding id.");
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Delete contract request", notes = "Deleting an existing contract request")
    @ApiResponses({
            @ApiResponse(code = 204, message = "Success"),
            @ApiResponse(code = 404, message = "Not found")
    })
    public void delete(
            @ApiParam(required = true, name = "id", value = "ID of the contract request you want to delete")
            @PathVariable Long id) {
        RBPMLogger logger = new RBPMLogger("Vertragsanfrage löschen", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        logger.log(ProcessState.STARTED);
        logger.log(ProcessState.WORK_IN_PROGRESS, "Vertragsanfrage löschen");
        contractRequestCollection.remove(id);
        logger.log(ProcessState.SUCCESSFULLY_DONE);
    }


    private Person createPerson(final String firstName, final String lastName, final String company) {

        Person person = new Person(firstName, lastName, company);
        Person savedPerson = null;
        try {
            String jsonValue = mapper.writeValueAsString(person);
            ResponseEntity<?> response = postData(CRM_URL, jsonValue);
            savedPerson = (Person) mapper.readValue((String) response.getBody(), Person.class);
            //savedPerson = person;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return savedPerson;
    }

    private Contract createContract(final Long salesmanId, final Long customerId, final String product) {

        Contract contract = new Contract(salesmanId, customerId, product);
        Contract savedContract = null;
        try {
            String jsonValue = mapper.writeValueAsString(contract);
            ResponseEntity<?> response = postData(CMS_URL, jsonValue);
            savedContract = (Contract) mapper.readValue(response.getBody().toString(), Contract.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return savedContract;
    }

    private ResponseEntity<?> postData(String appURL, String data) {
        try {

            //System.out.println(String.format("POST to url %s with request %s :: ", appURL, data));
            URL url = new URL(appURL);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            OutputStream os = con.getOutputStream();
            os.write(data.getBytes());
            os.flush();

            int responseCode = con.getResponseCode();
            System.out.println("POST Response Code :: " + responseCode);
            if (responseCode < HttpURLConnection.HTTP_BAD_REQUEST) { // success
                String responseBody = convertInputStream(con.getInputStream());
                return ResponseEntity.status(responseCode).body(responseBody);
            } else {
                String responseBody = convertInputStream(con.getErrorStream());
                return ResponseEntity.status(responseCode).body(responseBody);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Connection or data problem");
        }
    }

    private String convertInputStream(InputStream inputStream) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        // print result
        //System.out.println(response.toString());
        return response.toString();
    }

}
