package de.sidion.aim.cr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CrApp {

    public static void main(String[] args) {
        SpringApplication.run(CrApp.class, args);
    }
}
