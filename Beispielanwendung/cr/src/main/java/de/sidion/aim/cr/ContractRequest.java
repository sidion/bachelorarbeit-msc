package de.sidion.aim.cr;

public class ContractRequest {

    private Long id;
    private String salesmenFirstName;
    private String salesmenLastName;
    private String salesmanCompany;
    private String customerFirstName;
    private String customerLastName;
    private String customerCompany;
    private String productName;
    private String salesmenUrl;
    private String customerUrl;
    private String contractUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSalesmenFirstName() {
        return salesmenFirstName;
    }

    public void setSalesmenFirstName(String salesmenFirstName) {
        this.salesmenFirstName = salesmenFirstName;
    }

    public String getSalesmenLastName() {
        return salesmenLastName;
    }

    public void setSalesmenLastName(String salesmenLastName) {
        this.salesmenLastName = salesmenLastName;
    }

    public String getSalesmanCompany() {
        return salesmanCompany;
    }

    public void setSalesmanCompany(String salesmanCompany) {
        this.salesmanCompany = salesmanCompany;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public String getCustomerCompany() {
        return customerCompany;
    }

    public void setCustomerCompany(String customerCompany) {
        this.customerCompany = customerCompany;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSalesmenUrl() {
        return salesmenUrl;
    }

    public void setSalesmenUrl(String salesmenUrl) {
        this.salesmenUrl = salesmenUrl;
    }

    public String getCustomerUrl() {
        return customerUrl;
    }

    public void setCustomerUrl(String customerUrl) {
        this.customerUrl = customerUrl;
    }

    public String getContractUrl() {
        return contractUrl;
    }

    public void setContractUrl(String contractUrl) {
        this.contractUrl = contractUrl;
    }

}
