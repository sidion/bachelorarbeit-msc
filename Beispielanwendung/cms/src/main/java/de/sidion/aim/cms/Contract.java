package de.sidion.aim.cms;

public class Contract {

    private Long id;
    private Long salesmanId;
    private Long customerId;
    private String product;

    public Contract() {
    }

    public Contract(Long salesmanId, Long customerId, String product) {
        super();
        this.salesmanId = salesmanId;
        this.customerId = customerId;
        this.product = product;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "Contract [id=" + id + ", salesmanId=" + salesmanId + ", customerId=" + customerId + ", product="
                + product + "]";
    }


}
