package de.sidion.aim.cms;

import de.sidion.aim.rbpmlogging.ProcessState;
import de.sidion.aim.rbpmlogging.RBPMLogger;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/contract")
@Api("/contract")
public class CmsController {
    private Map<Long, Contract> contractCollection = new HashMap<>();

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Create contract", notes = "Creating a new contract", response = Contract.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = Contract.class)
    })
    public ResponseEntity<?> create(@RequestBody Contract contract) {
        RBPMLogger rbpmLogger = new RBPMLogger("Vertrag erstellen", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        rbpmLogger.log(ProcessState.STARTED);
        System.out.println("/contract POST Request received with data: " + contract);
        if (contract != null) {
            rbpmLogger.log(ProcessState.WORK_IN_PROGRESS, "Vertrag mit internen Infos anreichern");
            Long id = (long) contractCollection.size();
            contract.setId(id);
            rbpmLogger.log(ProcessState.WORK_IN_PROGRESS, "Vertrag speichern");
            contractCollection.put(contract.getId(), contract);
        }
        System.out.println("/contract POST Request done with data: " + contract);
        rbpmLogger.log(ProcessState.SUCCESSFULLY_DONE);
        return ResponseEntity.status(HttpStatus.CREATED).body(contract);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Find all contracts", notes = "Retrieving the collection of contracts", response = Contract[].class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = Contract[].class)
    })
    public ResponseEntity<?> getAll() {
        RBPMLogger logger = new RBPMLogger("Alle Verträge anzeigen", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        logger.log(ProcessState.STARTED);
        logger.log(ProcessState.WORK_IN_PROGRESS, "Verträge laden");
        logger.log(ProcessState.SUCCESSFULLY_DONE, "Verträge zurückgeben");
        return ResponseEntity.status(HttpStatus.OK).body(contractCollection.values());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> get(
            @ApiParam(required = true, name = "id", value = "ID of the contract you want to get")
            @PathVariable Long id) {
        RBPMLogger logger = new RBPMLogger("Einen Vertrag anzeigen", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        logger.log(ProcessState.STARTED);
        logger.log(ProcessState.WORK_IN_PROGRESS, "Passenden Vertragsanfrage suchen");
        if (contractCollection.containsKey(id)) {
            logger.log(ProcessState.SUCCESSFULLY_DONE, "Vertrag zurückgeben");
            return ResponseEntity.status(HttpStatus.OK).body(contractCollection.get(id));
        } else {
            logger.log(ProcessState.UNSUCCESSFULLY_DONE, "Vertrag nicht gefunden");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("There is no entry with corresponding id.");
        }
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Delete contract", notes = "Deleting an existing contract")
    @ApiResponses({
            @ApiResponse(code = 204, message = "Success"),
            @ApiResponse(code = 404, message = "Not found")
    })
    public void delete(
            @ApiParam(required = true, name = "id", value = "ID of the contract you want to delete")
            @PathVariable Long id) {
        RBPMLogger logger = new RBPMLogger("Vertrag löschen", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        logger.log(ProcessState.STARTED);
        logger.log(ProcessState.WORK_IN_PROGRESS, "Vertrag löschen");
        contractCollection.remove(id);
        logger.log(ProcessState.SUCCESSFULLY_DONE);
    }
}
