<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.0.0.RELEASE</version>
	</parent>

	<groupId>de.sidion.aim</groupId>
	<artifactId>cms</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>jar</packaging>

	<name>cms</name>
	<url>http://www.sidion.de</url>

	<properties>
		<port>8082</port>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<encoding>${project.build.sourceEncoding}</encoding>
					<compilerVersion>1.8</compilerVersion>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
			<!-- build docker image using http://dmp.fabric8.io/ for integration-tests -->
			<!-- this works, but it is currently not used during build, cause we may not have docker now -->
			<!-- so it is set to execution phase "none" to get ignored -->
			<plugin>
				<groupId>io.fabric8</groupId>
				<artifactId>docker-maven-plugin</artifactId>
				<version>0.33.0</version>

				<configuration>
					<dockerHost>http://localhost:2375</dockerHost>
					<useColor>true</useColor>
					<images>
						<image>
							<!-- name>sidion/cms-app:0.0.1-latest</name -->
							<name>sidion/${project.artifactId}-app:%l</name>
							<alias>${project.artifactId}-app</alias>
							<build>
								<contextDir>${project.basedir}</contextDir>
								<dockerFile>Dockerfile</dockerFile>
							</build>
							<run>
								<env>
								</env>
								<labels>
									<environment>development</environment>
									<version>${project.version}</version>
								</labels>
								<ports>
									<port>${port}:${port}</port>
								</ports>
								<wait>
									<http>
										<url>http://localhost:${port}/v2/api-docs?group=${project.artifactId}-api</url>
										<method>GET</method>
										<status>200</status>
									</http>
									<time>30000</time>
								</wait>
								<log>
									<prefix>DEMO</prefix>
									<date>ISO8601</date>
									<color>blue</color>
								</log>
							</run>
						</image>
					</images>
				</configuration>
				<executions>
					<!-- Connect to phase "pre-integration-test", if you use the docker container during integration tests -->
					<execution>
						<id>start</id>
						<!-- phase>pre-integration-test</phase -->
						<phase>none</phase>
						<goals>
							<!-- "build" should be used to create the images with the artifact -->
							<goal>build</goal>
							<goal>start</goal>
						</goals>
					</execution>
					<!-- Connect to phase "post-integration-test", if you use the docker container during integration tests -->
					<execution>
						<id>stop</id>
						<!-- phase>post-integration-test</phase -->
						<phase>none</phase>
						<goals>
							<goal>stop</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<!-- use docker image for integration-tests to download swagger file -->
			<!-- https://github.com/maven-download-plugin/maven-download-plugin -->
			<!-- this works, but it is currently not used during build, cause we may not have docker now -->
			<!-- so it is set to execution phase "none" to get ignored -->
			<plugin>
				<groupId>com.googlecode.maven-download-plugin</groupId>
				<artifactId>download-maven-plugin</artifactId>
				<version>1.5.0</version>
				<executions>
					<execution>
						<id>download-swagger-file</id>
						<!-- Connect to phase "integration-test", if you use the docker container during integration tests -->
						<!-- phase>integration-test</phase -->
						<phase>none</phase>
						<goals>
							<goal>wget</goal>
						</goals>
						<configuration>
							<url>http://localhost:${port}/v2/api-docs?group=${project.artifactId}-api</url>
							<unpack>false</unpack>
							<overwrite>true</overwrite>
							<skipCache>true</skipCache>
							<outputFileName>${project.artifactId}-app.swagger.json</outputFileName>
							<outputDirectory>${project.build.directory}</outputDirectory>
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
	<dependencies>
		<!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-web -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-log4j2 -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-log4j2</artifactId>
			<version>2.1.2.RELEASE</version>
		</dependency>

		<!-- rbpm-event library -->
		<dependency>
			<groupId>de.sidion.aim</groupId>
			<artifactId>RBPM-Logging</artifactId>
			<version>0.0.13-SNAPSHOT</version>
		</dependency>
		
		<!--  Swagger -->
		<!--  https://github.com/springfox/springfox/issues/2265 -->
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>2.9.2</version>
			<exclusions>
				<exclusion>
					<groupId>io.swagger</groupId>
					<artifactId>swagger-annotations</artifactId>
				</exclusion>
				<exclusion>
					<groupId>io.swagger</groupId>
					<artifactId>swagger-models</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>2.9.2</version>
		</dependency>
		<dependency>
			<groupId>io.swagger</groupId>
			<artifactId>swagger-annotations</artifactId>
			<version>1.5.21</version>
		</dependency>
		<dependency>
			<groupId>io.swagger</groupId>
			<artifactId>swagger-models</artifactId>
			<version>1.5.21</version>
		</dependency>
		
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>
</project>