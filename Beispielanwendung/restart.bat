docker-compose down
cd ..\LoggingBibliothek
call mvn install
@Echo on
cd ..\Beispielanwendung\cms
call mvn package -DskipTests
@Echo on
cd ..\cr
call mvn package -DskipTests
@Echo on
cd ..\crm
call mvn package -DskipTests
@Echo on
cd ..\..\KafkaConnect\JSONStringToObject
call gradlew jar
@Echo on
copy /y build\libs\JSONStringToObject-0.0.2-SNAPSHOT.jar ..\..\Beispielanwendung\file-connect\connect\plugins
cd ..\..\Beispielanwendung
docker volume rm beispielanwendung_logs
del /q C:\Volumes\logs\*
docker-compose up --build -d
