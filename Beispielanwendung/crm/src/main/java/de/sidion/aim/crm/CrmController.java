package de.sidion.aim.crm;

import de.sidion.aim.rbpmlogging.ProcessState;
import de.sidion.aim.rbpmlogging.RBPMLogger;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/person")
public class CrmController {
    private final Map<Long, Person> personCollection = new HashMap<>();

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<?> create(@Valid @RequestBody Person person) {
        RBPMLogger logger = new RBPMLogger("Person erstellen", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        logger.log(ProcessState.STARTED);
        System.out.printf("/person POST Request recieved with data: %s\n", person);
        if (person != null) {
            Long id = (long) personCollection.size();
            logger.log(ProcessState.WORK_IN_PROGRESS, "Person mit internen Informationen anreichern");
            person.setId(id);
            logger.log(ProcessState.WORK_IN_PROGRESS, "Person zu Liste hinzufügen");
            personCollection.put(person.getId(), person);
        }
        System.out.printf("/person POST Request done with data: %s\n", person);
        logger.log(ProcessState.SUCCESSFULLY_DONE);
        return ResponseEntity.status(HttpStatus.CREATED).body(person);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<?> getAll() {
        RBPMLogger logger = new RBPMLogger("Alle Personen anzeigen", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        logger.log(ProcessState.STARTED);
        logger.log(ProcessState.WORK_IN_PROGRESS, "Personen laden");
        logger.log(ProcessState.SUCCESSFULLY_DONE, "Personen zurückgeben");
        return ResponseEntity.status(HttpStatus.OK).body(personCollection.values());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> get(
            @ApiParam(required = true, name = "id", value = "ID of the person you want to get")
            @PathVariable Long id) {
        RBPMLogger logger = new RBPMLogger("Eine Person anzeigen", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        logger.log(ProcessState.STARTED);
        logger.log(ProcessState.WORK_IN_PROGRESS, "Passende Person suchen");
        if (personCollection.containsKey(id)) {
            logger.log(ProcessState.SUCCESSFULLY_DONE, "Person zurückgeben");
            return ResponseEntity.status(HttpStatus.OK).body(personCollection.get(id));
        } else {
            logger.log(ProcessState.UNSUCCESSFULLY_DONE, "Person nicht gefunden");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("There is no entry with corresponding id.");
        }
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Delete person", notes = "Deleting an existing person")
    @ApiResponses({
            @ApiResponse(code = 204, message = "Success"),
            @ApiResponse(code = 404, message = "Not found")
    })
    public void delete(
            @ApiParam(required = true, name = "id", value = "ID of the person you want to delete")
            @PathVariable Long id) {
        RBPMLogger logger = new RBPMLogger("Person löschen", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        logger.log(ProcessState.STARTED);
        logger.log(ProcessState.WORK_IN_PROGRESS, "Person löschen");
        personCollection.remove(id);
        logger.log(ProcessState.SUCCESSFULLY_DONE);
    }

}
