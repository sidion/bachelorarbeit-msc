package de.sidion.aim.crm;

public enum Gender {
    MALE, FEMALE, DIVERSE
}
