package de.sidion.aim.crm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;

@SpringBootApplication
@Import(BeanValidatorPluginsConfiguration.class)
public class CrmApp {
    public static void main(String[] args) {
        SpringApplication.run(CrmApp.class, args);
    }
}
