package de.sidion.aim.crm;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.*;

public class Person {

    private Long id;

    @ApiModelProperty(notes = "First Name of the person.",
            example = "Azmir", required = true, position = 1)
    @NotNull
    @NotBlank
    @Size(min = 2, max = 25)
    private String firstName;

    @ApiModelProperty(notes = "Last Name of the person.",
            example = "Abdi", required = true, position = 2)
    @NotNull
    @NotBlank
    @Size(min = 2, max = 25)
    private String lastName;

    @ApiModelProperty(notes = "Company of the person.",
            example = "sidion", required = false, position = 3)
    private String company;

    @ApiModelProperty(notes = "Age of the person.",
            example = "59", required = false, position = 4)
    @Min(18)
    @Max(99)
    private Integer age;

    @ApiModelProperty(notes = "Email address of the person.",
            example = "azmir.abdi@sidion.de", required = false, position = 5)
    @Email(message = "Email Address")
    @Size(min = 6, max = 25)
    @Pattern(regexp = "(.+)@(.+)", message = "Please provide a valid email adress")
    private String email;

    //@CreditCardNumber
    private String creditCardNumber;

    //@UniqueElements
    private Gender gender;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Person [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", company=" + company
                + ", age=" + age + ", email=" + email + ", creditCardNumber=" + creditCardNumber + ", gender=" + gender
                + "]";
    }

}
