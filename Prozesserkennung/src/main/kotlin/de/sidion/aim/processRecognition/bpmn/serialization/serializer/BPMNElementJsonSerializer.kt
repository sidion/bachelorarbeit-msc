package de.sidion.aim.processRecognition.bpmn.serialization.serializer

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JavaType
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import de.sidion.aim.processRecognition.bpmn.definition.BPMNElement
import de.sidion.aim.processRecognition.bpmn.definition.CollectingGateway
import de.sidion.aim.processRecognition.bpmn.definition.HasBefore

class BPMNElementJsonSerializer<T: BPMNElement>(clazz: Class<T>) : JsonSerializer<T>() {
    init {
        val module = SimpleModule()
        module.addSerializer(clazz,
            EventSerializer(
                clazz
            )
        )
        mapper.registerModule(module)
    }

    private class EventSerializer<T: BPMNElement> : StdSerializer<T> {
        constructor(t: Class<T>) : super(t)
        constructor(type: JavaType?) : super(type)
        constructor(t: Class<*>?, dummy: Boolean) : super(t, dummy)
        constructor(src: StdSerializer<*>?) : super(src)

        override fun serialize(value: T, gen: JsonGenerator, provider: SerializerProvider?) {
            writeObject(value, gen)
        }

        private fun writeObject(value: BPMNElement?, gen: JsonGenerator) {
            gen.writeStartObject()
            if (value != null) {
                gen.writeStringField("class", value::class.simpleName)
                gen.writeStringField("processName", value.processName)
                gen.writeStringField("instanceId", value.instanceId)
                gen.writeStringField("stepName", value.stepName)
                gen.writeStringField("timestamp", value.timestamp)
                gen.writeBooleanField("instance", value.isInstance())
                gen.writeArrayFieldStart("pre")
                when (value) {
                    is HasBefore -> {
                        writeObject(value.getBefore(), gen)
                    }
                    is CollectingGateway -> value.before.forEach { writeObject(it, gen) }
                }
                gen.writeEndArray()
                val id: Long = try {
                    value.getId()
                } catch (ex: UninitializedPropertyAccessException) {
                    -1
                }
                gen.writeNumberField("id", id)
            }
            gen.writeEndObject()
        }
    }
}
