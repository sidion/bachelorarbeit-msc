package de.sidion.aim.processRecognition.neo4j.services

import de.sidion.aim.processRecognition.neo4j.Entity
import de.sidion.aim.processRecognition.neo4j.Neo4JSessionFactory

abstract class GenericService<T : Entity> : Service<T> {
    private val session = Neo4JSessionFactory.getSession()
    override fun findAll(): Iterable<T> {
        return session.loadAll(getEntityType())
    }

    override fun findAllByProcessName(): Iterable<T> {
        TODO("Not yet implemented")
    }

    override fun find(id: Long): T {
        return session.load(getEntityType(), id)
    }

    fun test(): Iterable<T> {
        return session.query(getEntityType(), "", mapOf<String, Any>())
    }

    override fun delete(id: Long) {
        delete(find(id))
    }

    override fun delete(element: T) {
        session.delete(element)
    }

    override fun save(element: T): T {
        session.save(element)
        return find(element.getId())
    }

    protected abstract fun getEntityType(): Class<T>
}