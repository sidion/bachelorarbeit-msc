package de.sidion.aim.processRecognition

data class Config(
    val kafka: Kafka,
    val neo4J: Neo4J
) {
    data class Neo4J(
        val uri: String,
        val user: String,
        val password: String
    )
    data class Kafka(
        val bootstrapServers: String,
        val schemaRegistryUrl: String,
        val topics: Topics
    ){
        data class Topics (
            val fromConnect: String,
            val internal: String,
            val forInstance: String,
            val forProcess: String
        )
    }
}