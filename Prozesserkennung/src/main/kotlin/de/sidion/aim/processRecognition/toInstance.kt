package de.sidion.aim.processRecognition

import de.sidion.aim.EventKey
import de.sidion.aim.LogEntryKey
import de.sidion.aim.processRecognition.bpmn.definition.BPMNElement
import de.sidion.aim.processRecognition.bpmn.definition.EndEvent
import de.sidion.aim.processRecognition.bpmn.definition.HasAfter
import de.sidion.aim.processRecognition.bpmn.definition.HasBefore
import de.sidion.aim.processRecognition.neo4j.services.BasicService
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Consumed
import org.apache.kafka.streams.kstream.Produced
import org.apache.kafka.streams.kstream.SessionWindows
import java.time.Duration

fun combineProcessInstance(
    keySerde: Serde<LogEntryKey>,
    valueSerde: Serde<BPMNElement>,
    newKeySerde: Serde<EventKey>,
    newValSerde: Serde<EndEvent>
): Topology {
    val builder = StreamsBuilder()
    val stream = builder.stream(getConfig().kafka.topics.internal, Consumed.with(keySerde, valueSerde))

    val windowed = stream.groupByKey().windowedBy(SessionWindows.with(Duration.ofSeconds(5)))
    val reduced = windowed.reduce { event, event2 ->
        if (event == event2) return@reduce event
        if (event.timestamp < event2.timestamp || event is HasAfter && event2 is HasBefore) {
            setConnection(event, event2)
            return@reduce event2
        } else {
            setConnection(event2, event)
            return@reduce event
        }
    }.toStream()

    val saveService = BasicService()

    reduced.foreach { _, event ->
        saveService.save(event)
        getLogger(Main::class).info("saved $event")
    }

    reduced.map { windowedKey, event ->
        val key = EventKey()
        key.setProcessName(windowedKey.key().getProcessName())
        KeyValue(key, event.getEndEvent()!!)
    }.to(getConfig().kafka.topics.forInstance, Produced.with(newKeySerde, newValSerde))

    return builder.build()
}

fun setConnection(firstStep: BPMNElement, secondStep: BPMNElement) {
    val ev = firstStep as? HasAfter ?: throw IllegalArgumentException(firstStep.toString())
    val ev2 = secondStep as? HasBefore ?: throw IllegalArgumentException(secondStep.toString())
    ev.setAfter(secondStep)
    ev2.setBefore(firstStep)
}
