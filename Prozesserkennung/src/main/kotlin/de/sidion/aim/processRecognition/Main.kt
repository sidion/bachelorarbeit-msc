package de.sidion.aim.processRecognition

import de.sidion.aim.EventKey
import de.sidion.aim.LogEntryFromConnect
import de.sidion.aim.LogEntryKey
import de.sidion.aim.processRecognition.bpmn.definition.*
import de.sidion.aim.processRecognition.bpmn.serialization.BPMNElementSerde
import de.sidion.aim.processRecognition.bpmn.serialization.ProcessSerde
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.StreamsConfig
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*
import kotlin.reflect.KClass
import kotlin.system.exitProcess

class Main {
    companion object {
        @JvmStatic
        var config: Config? = null
    }
}

fun main() {
    val logger = getLogger(Main::class)

    createConfig()
//    val service = BasicService()
    startStream()
}

fun createProcess(process: String, vararg steps: String): StartedEvent {
    val start = StartedEvent(false)
    start.processName = process
    var cur = start as BPMNElement
    for (step in steps) {
        val activity = Activity(false)
        activity.processName = process
        activity.stepName = step
        (cur as HasAfter).setAfter(activity)
        activity.setBefore(cur)
        cur = activity
    }
    val end = SuccessfulEndEvent(false)
    end.processName = process
    end.setBefore(cur)
    (cur as HasAfter).setAfter(end)
    return start
}

fun createConfig() {
    val logger = getLogger(Main::class)
    if (Main.config == null) {
        val config = Config(
            Config.Kafka(
                System.getenv("KAFKA_BOOTSTRAP_SERVERS") ?: "",
                System.getenv("SCHEMA_REGISTRY_URL") ?: "",
                Config.Kafka.Topics(
                    System.getenv("KAFKA_TOPIC_FROM_CONNECT") ?: "",
                    System.getenv("KAFKA_TOPIC_INTERNAL_STORE") ?: "",
                    System.getenv("KAFKA_TOPIC_FOR_INSTANCE") ?: "",
                    System.getenv("KAFKA_TOPIC_FOR_PROCESS") ?: ""
                )
            ),
            Config.Neo4J(
                System.getenv("NEO4J_URI") ?: "",
                System.getenv("NEO4J_USER") ?: "",
                System.getenv("NEO4J_PASSWORD") ?: ""
            )
        )
        Main.config = config
        if (config.kafka.bootstrapServers == "") {
            logger.error("Environment variable KAFKA_BOOTSTRAP_SERVERS has to be set")
            Main.config = null
        }
        if (config.kafka.schemaRegistryUrl == "") {
            logger.error("Environment variable SCHEMA_REGISTRY_URL has to be set")
            Main.config = null
        }
        if (config.kafka.topics.fromConnect == "") {
            logger.error("Environment variable KAFKA_TOPIC_FROM_CONNECT has to be set")
            Main.config = null
        }
        if (config.kafka.topics.internal == "") {
            logger.error("Environment variable KAFKA_TOPIC_INTERNAL_STORE has to be set")
            Main.config = null
        }
        if (config.kafka.topics.forInstance == "") {
            logger.error("Environment variable KAFKA_TOPIC_FOR_INSTANCE has to be set")
            Main.config = null
        }
        if (config.kafka.topics.forProcess == "") {
            logger.error("Environment variable KAFKA_TOPIC_FOR_PROCESS has to be set")
            Main.config = null
        }

        if (config.neo4J.uri == "") {
            logger.error("Environment variable NEO4J_URI has to be set")
            Main.config = null
        }
        if (config.neo4J.user == "") {
            logger.error("Environment variable NEO4J_USER has to be set")
            Main.config = null
        }
        if (config.neo4J.password == "") {
            logger.error("Environment variable NEO4J_PASSWORD has to be set")
            Main.config = null
        }
        if (Main.config == null) {
            exitProcess(1)
        }
    }
}

fun getConfig(): Config {
    return Main.config ?: throw NullPointerException()
}

fun startStream() {
    val streamsConfigBase = Properties()
    streamsConfigBase[StreamsConfig.BOOTSTRAP_SERVERS_CONFIG] = getConfig().kafka.bootstrapServers
    streamsConfigBase["schema.registry.url"] = getConfig().kafka.schemaRegistryUrl

    val serdeConfig = mapOf("schema.registry.url" to getConfig().kafka.schemaRegistryUrl)
    val keySerde = SpecificAvroSerde<LogEntryKey>()
    keySerde.configure(serdeConfig, true)
    val valSerde = SpecificAvroSerde<LogEntryFromConnect>()
    valSerde.configure(serdeConfig, false)

    val newKeySerde = SpecificAvroSerde<EventKey>()
    newKeySerde.configure(serdeConfig, true)

    val bpmnElementSerde = BPMNElementSerde()
    val processSerde = ProcessSerde()

    val toEventTopology = convertEntryToEvent(keySerde, valSerde, bpmnElementSerde)
    val processInstance = combineProcessInstance(keySerde, bpmnElementSerde, newKeySerde, processSerde)
    val processModel = combineProcess(newKeySerde, processSerde)

    val eventConfig = streamsConfigBase.clone() as Properties
    eventConfig[StreamsConfig.APPLICATION_ID_CONFIG] = "process-recognition-convert"
    eventConfig[StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG] = SpecificAvroSerde::class.java
    eventConfig[StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG] = SpecificAvroSerde::class.java
    val toEvent = KafkaStreams(toEventTopology, eventConfig)
    val instanceCreationConfig = streamsConfigBase.clone() as Properties
    instanceCreationConfig[StreamsConfig.APPLICATION_ID_CONFIG] = "process-recognition-instance-creation"
    instanceCreationConfig[StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG] = SpecificAvroSerde::class.java
    instanceCreationConfig[StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG] = BPMNElementSerde::class.java
    val createInstance = KafkaStreams(processInstance, instanceCreationConfig)
    val processModelConfig = streamsConfigBase.clone() as Properties
    processModelConfig[StreamsConfig.APPLICATION_ID_CONFIG] = "process-recognition-model-creation"
    processModelConfig[StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG] = SpecificAvroSerde::class.java
    processModelConfig[StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG] = BPMNElementSerde::class.java
    val createProcessModell = KafkaStreams(processModel, processModelConfig)

    toEvent.start()
    createInstance.start()
    createProcessModell.start()

    Runtime.getRuntime().addShutdownHook(object : Thread("streams shutdown hook") {
        override fun run() {
            toEvent.close()
            createInstance.close()
            createProcessModell.close()
        }
    })
}

fun getLogger(forClass: Class<*>): Logger = LoggerFactory.getLogger(forClass)
fun getLogger(forClass: KClass<*>): Logger = LoggerFactory.getLogger(forClass.java)
