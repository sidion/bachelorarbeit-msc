package de.sidion.aim.processRecognition.bpmn.serialization.serializer

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JavaType
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import de.sidion.aim.processRecognition.bpmn.definition.*
import de.sidion.aim.processRecognition.neo4j.services.BasicService

abstract class BPMNElementJsonDeserializer<T: BPMNElement>(clazz: Class<T>) : JsonDeserializer<T>() {
    init {
        val module = SimpleModule()
        module.addDeserializer(clazz,
            EventDeserializer(
                clazz
            )
        )
        mapper.registerModule(module)
    }

    private class EventDeserializer<T: BPMNElement> : StdDeserializer<T> {
        constructor(vc: Class<T>) : super(vc)
        constructor(valueType: JavaType?) : super(valueType)
        constructor(src: StdDeserializer<*>?) : super(src)

        override fun deserialize(p: JsonParser, ctxt: DeserializationContext): T {
            val node = p.codec.readTree<JsonNode>(p)
            val mapped = mapToEvent(node)
            return mapped as? T ?: mapped.getEndEvent() as T
        }

        private fun mapToEvent(node: JsonNode): BPMNElement {
            val id = node.get("id").asLong(-1)
            val event = if (id != -1L) {
                val service = BasicService()
                when (val classType = node.get("class").asText()) {
                    "Activity" -> service.find(id, Activity::class.java)
                    "StartedEvent" -> service.find(id, StartedEvent::class.java)
                    "SuccessfulEndEvent" -> service.find(id, SuccessfulEndEvent::class.java)
                    "UnsuccessfulEndEvent" -> service.find(id, UnsuccessfulEndEvent::class.java)
                    "ExceptionEndEvent" -> service.find(id, ExceptionEndEvent::class.java)
                    "ExclusiveGateway" -> service.find(id, ExclusiveGateway::class.java)
                    "ParallelGateway" -> service.find(id, ParallelGateway::class.java)
                    "CollectingGateway" -> service.find(id, CollectingGateway::class.java)
                    else -> throw IllegalArgumentException("wrong type given: $classType")
                }
            } else {
                val isInstance = node.get("instance").asBoolean()
                when (val classType = node.get("class").asText()) {
                    "Activity" -> Activity(isInstance)
                    "StartedEvent" -> StartedEvent(isInstance)
                    "SuccessfulEndEvent" -> SuccessfulEndEvent(isInstance)
                    "UnsuccessfulEndEvent" -> UnsuccessfulEndEvent(isInstance)
                    "ExceptionEndEvent" -> ExceptionEndEvent(isInstance)
                    "ExclusiveGateway" -> ExclusiveGateway(isInstance)
                    "ParallelGateway" -> ParallelGateway(isInstance)
                    "CollectingGateway" -> CollectingGateway(isInstance)
                    else -> throw IllegalArgumentException("wrong type given: $classType")
                }
            }
            event.timestamp = node.get("timestamp").asText()
            event.processName = node.get("processName").asText()
            event.instanceId = node.get("instanceId").asText()
            event.stepName = node.get("stepName").asText()
            node.get("pre").forEach {
                it.get("id")?.asLong() ?: return@forEach
                val pre = mapToEvent(it)
                when (event) {
                    is HasBefore -> event.setBefore(pre)
                    is CollectingGateway -> {
                        if (event.before.isNotEmpty()) {
                            val splitting = getMatchingSplit(event)!!
                            val toCollect = CollectingGateway()
                            toCollect.before.add(pre)
                            toCollect.stepName = splitting.stepName
                            val afterOtherSplit = getMatchingSplit(toCollect)!!.after.first()
                            (afterOtherSplit as HasBefore).setBefore(splitting)
                            splitting.after.add(afterOtherSplit)
                        }
                        event.before.add(pre)
                    }
                }
                when (pre) {
                    is HasAfter -> pre.setAfter(event)
                    is SplittingGateways -> pre.after.add(event)
                }
            }
            return event
        }

        private fun getMatchingSplit(start: CollectingGateway): SplittingGateways? {
            var elem = start.before.firstOrNull()
            while (elem != null && elem !is SplittingGateways && elem.stepName != start.stepName) {
                elem = when (elem) {
                    is HasBefore -> elem.getBefore()
                    is CollectingGateway -> getMatchingSplit(elem)?.getBefore()
                    else -> null
                }
            }
            return elem as? SplittingGateways
        }
    }
}
