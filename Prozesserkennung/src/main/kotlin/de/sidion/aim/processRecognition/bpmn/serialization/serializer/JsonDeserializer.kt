package de.sidion.aim.processRecognition.bpmn.serialization.serializer

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.kafka.common.errors.SerializationException
import org.apache.kafka.common.serialization.Deserializer


abstract class JsonDeserializer<T> : Deserializer<T> {
    protected val mapper = ObjectMapper()

    override fun deserialize(p0: String?, p1: ByteArray?): T {
        try {
            return mapper.readValue(p1, getEntityType())
        } catch (ex: Exception) {
            throw SerializationException("Error Deserializing Object", ex)
        }
    }

    abstract fun getEntityType(): TypeReference<T>
}
