package de.sidion.aim.processRecognition.bpmn.definition

import org.neo4j.ogm.annotation.NodeEntity

@NodeEntity
class Activity : Activities {
    constructor(): super(true)
    constructor(isInstance: Boolean): super(isInstance)

    override fun getStartEvent() = getBefore()?.getStartEvent()
    override fun getEndEvent() = getAfter()?.getEndEvent()
    override fun getAsProcessElement() = Activity(false)
}