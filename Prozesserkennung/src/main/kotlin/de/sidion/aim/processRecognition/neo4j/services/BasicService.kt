package de.sidion.aim.processRecognition.neo4j.services

import de.sidion.aim.processRecognition.bpmn.definition.BPMNElement
import de.sidion.aim.processRecognition.bpmn.definition.EndEvent
import de.sidion.aim.processRecognition.bpmn.definition.SuccessfulEndEvent
import de.sidion.aim.processRecognition.neo4j.Neo4JSessionFactory

open class BasicService {
    private val session = Neo4JSessionFactory.getSession()

    fun <T : BPMNElement> find(id: Long, clazz: Class<T>): T {
        return session.load(clazz, id)
    }

    fun <T : BPMNElement> save(elem: T) {
        session.save(elem)
    }

    fun <T : BPMNElement> save(elem: T, clazz: Class<T>): T {
        save(elem)
        return find(elem.getId(), clazz)
    }

    fun getProcess(process: String, endType: String): EndEvent? {
        return session.queryForObject(
            EndEvent::class.java,
            "match (getMax:$endType {isInstance: false, processName: \$processName}) " +
                    "with MAX(getMax.timestamp) as maxTimestamp " +
                    "match p=(getMax)<-[*]-(:StartedEvent) " +
                    "where getMax.timestamp = maxTimestamp " +
                    "return p",
            mapOf(
                "processName" to process
            )
        )
    }
}
