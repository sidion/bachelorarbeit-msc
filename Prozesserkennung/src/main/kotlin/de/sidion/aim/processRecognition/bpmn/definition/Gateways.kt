package de.sidion.aim.processRecognition.bpmn.definition

import org.neo4j.ogm.annotation.NodeEntity
import org.neo4j.ogm.annotation.Relationship

abstract class SplittingGateways(isInstance: Boolean) : Gateways(isInstance), HasBefore {
    @Relationship(type = "TRIGGER", direction = Relationship.INCOMING)
    private var before: BPMNElement? = null
    @Relationship(type = "TRIGGER")
    val after = mutableSetOf<BPMNElement>()

    override fun getStartEvent() = before?.getStartEvent()
    override fun getEndEvent() = after.firstOrNull()?.getEndEvent()
    override fun getBefore() = before

    override fun setBefore(before: BPMNElement) {
        if (before == this) {
            throw IllegalArgumentException("You could not be your own before step")
        }
        this.before = before
    }
}

@NodeEntity
class ExclusiveGateway : SplittingGateways {
    constructor(): super(true)
    constructor(isInstance: Boolean): super(isInstance)
    override fun getAsProcessElement() = ExclusiveGateway(false)
}

@NodeEntity
class ParallelGateway : SplittingGateways {
    constructor(): super(true)
    constructor(isInstance: Boolean): super(isInstance)
    override fun getAsProcessElement() = ParallelGateway(false)
}

@NodeEntity
class CollectingGateway : Gateways, HasAfter {
    constructor(): super(true)
    constructor(isInstance: Boolean): super(isInstance)
    @Relationship(type = "TRIGGER", direction = Relationship.INCOMING)
    val before = mutableSetOf<BPMNElement>()
    @Relationship(type = "TRIGGER")
    private var after: BPMNElement? = null

    override fun getAfter() = after
    override fun setAfter(after: BPMNElement) {
        if (after == this) {
            throw IllegalArgumentException("You could not be your own after step")
        }
        this.after = after
    }
    override fun getStartEvent() = before.firstOrNull()?.getStartEvent()
    override fun getEndEvent() = after?.getEndEvent()
    override fun getAsProcessElement() = CollectingGateway(false)
}
