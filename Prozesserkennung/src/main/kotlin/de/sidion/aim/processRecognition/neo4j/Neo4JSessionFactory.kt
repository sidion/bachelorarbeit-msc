package de.sidion.aim.processRecognition.neo4j

import de.sidion.aim.processRecognition.getConfig
import de.sidion.aim.processRecognition.getLogger
import org.neo4j.ogm.config.Configuration
import org.neo4j.ogm.session.Session
import org.neo4j.ogm.session.SessionFactory

class Neo4JSessionFactory {
    companion object {
        @JvmStatic
        private val config: Configuration = Configuration.Builder()
            .uri(getConfig().neo4J.uri)
            .credentials(getConfig().neo4J.user, getConfig().neo4J.password)
            .build()

        @JvmStatic
        private val sessionFactory = SessionFactory(config, "de.sidion.aim.processRecognition.bpmn.definition")
        private val sessionList = mutableSetOf<Session>()

        @JvmStatic
        fun getSession(): Session {
            val session = sessionFactory.openSession()
            sessionList.add(session)
            return session
        }
    }
}