package de.sidion.aim.processRecognition

import de.sidion.aim.LogEntryFromConnect
import de.sidion.aim.LogEntryKey
import de.sidion.aim.ProcessState
import de.sidion.aim.processRecognition.bpmn.definition.*
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Consumed
import org.apache.kafka.streams.kstream.Produced

fun convertEntryToEvent(
    keyAvroSerde: Serde<LogEntryKey>,
    valAvroSerde: Serde<LogEntryFromConnect>,
    jsonSerde: Serde<BPMNElement>
): Topology {
    val builder = StreamsBuilder()
    val streamToEvent = builder.stream(getConfig().kafka.topics.fromConnect, Consumed.with(keyAvroSerde, valAvroSerde))
    val correctObject = streamToEvent.mapValues { value ->
        val event = when (ProcessState.valueOf(value.getState())) {
            ProcessState.STARTED -> StartedEvent(true)
            ProcessState.WORK_IN_PROGRESS -> Activity(true)
            ProcessState.EXCEPTION_OCCURRED -> ExceptionEndEvent(true)
            ProcessState.SUCCESSFULLY_DONE -> SuccessfulEndEvent(true)
            ProcessState.UNSUCCESSFULLY_DONE -> UnsuccessfulEndEvent(true)
        }
        event.instanceId = value.getInstanceId()
        event.processName = value.getProcessName()
        event.stepName = value.getStepName()
        event.timestamp = value.getTimestamp()
        return@mapValues event
    }
    correctObject.to(getConfig().kafka.topics.internal, Produced.with(keyAvroSerde, jsonSerde))
    return builder.build()
}
