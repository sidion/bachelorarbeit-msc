package de.sidion.aim.processRecognition

import de.sidion.aim.EventKey
import de.sidion.aim.processRecognition.bpmn.definition.*
import de.sidion.aim.processRecognition.neo4j.services.BasicService
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Consumed
import org.apache.kafka.streams.kstream.SessionWindows
import java.time.Duration
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


fun combineProcess(
    keySerde: Serde<EventKey>,
    valueSerde: Serde<EndEvent>
): Topology {
    val service = BasicService()
    val builder = StreamsBuilder()
    val stream = builder.stream(getConfig().kafka.topics.forInstance, Consumed.with(keySerde, valueSerde))

    val filtered = stream.filter { _, bpmnElement ->
        bpmnElement.getStartEvent() ?: return@filter false
        true
    }
    val toProcess = filtered
        .mapValues { instance ->
            return@mapValues copyToProcess(instance.getStartEvent()!!).getEndEvent()!!
        }
    val grouped = toProcess.groupByKey().windowedBy(SessionWindows.with(Duration.ofSeconds(5)))
    val reduced = grouped.reduce { process, process2 ->
        getLogger(Main::class).info("reduce $process")
        return@reduce combineTwo(process.getStartEvent()!!, process2.getStartEvent()!!, 0).getEndEvent()!!
    }.toStream()

    val dbChecked =
        reduced.filter { _, process ->
            if (process == null) return@filter false
            val fromDB = service.getProcess(process.processName, process::class.simpleName!!) ?: return@filter true
            fromDB.timestamp=""
            !inDBProcess(process, fromDB)
        }.mapValues { endEvent ->
            endEvent.timestamp  = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME)
            endEvent
        }

    dbChecked.foreach { _, bpmnElement ->
        service.save(bpmnElement)
        getLogger(Main::class).info("saved process $bpmnElement")
    }

    dbChecked.map { windowed, endEvent ->
        KeyValue(windowed.key(), endEvent)
    }.to(getConfig().kafka.topics.forProcess)

    return builder.build()
}

fun combineTwo(process: BPMNElement, process2: BPMNElement, splitDepth: Int): BPMNElement {
    if (process == process2) {
        return if (process is HasAfter && process2 is HasAfter) {
            val next1 = process.getAfter()!!
            val next2 = process2.getAfter()!!
            val copied = process.toProcessElement()
            val end = combineTwo(next1, next2, splitDepth)
            (copied as HasAfter).setAfter(end)
            (end as HasBefore).setBefore(copied)
            copied
        } else if (process is SplittingGateways && process2 is SplittingGateways) {
            val start = process.toProcessElement() as SplittingGateways
            val collecting = copySplittingToCollecting(start, process)
            process2.after.forEach { addNewPath(start, it) }
            val next1 = getCollectingElement(process).getAfter()!!
            val next2 = getCollectingElement(process2).getAfter()!!
            val end = combineTwo(next1, next2, splitDepth)
            collecting.setAfter(end)
            (end as HasBefore).setBefore(collecting)
            start
        } else {
            process.toProcessElement()
        }
    } else {
        return if (process is HasAfter && process2 is HasAfter) {
            val split = ExclusiveGateway(false)
            split.processName = process.processName
            split.stepName = "Gateway $splitDepth"
            var path1 = process.toProcessElement()
            var path2 = process2.toProcessElement()
            split.after.add(path1)
            (path1 as HasBefore).setBefore(split)
            split.after.add(path2)
            (path2 as HasBefore).setBefore(split)
            var next1 = process.getAfter()!!
            var next2 = process2.getAfter()!!
            while (next1 != next2) {
                val cur1 = next1.toProcessElement()
                (path1 as HasAfter).setAfter(cur1)
                (cur1 as HasBefore).setBefore(path1)
                path1 = cur1

                val cur2 = next2.toProcessElement()
                (path2 as HasAfter).setAfter(cur2)
                (cur2 as HasBefore).setBefore(path2)
                path2 = cur2

                next1 = (next1 as HasAfter).getAfter()!!
                next2 = (next2 as HasAfter).getAfter()!!
            }
            val collect = CollectingGateway(false)
            collect.processName = process.processName
            collect.stepName = "Gateway $splitDepth"
            collect.before.add(path1)
            (path1 as HasAfter).setAfter(collect)
            collect.before.add(path2)
            (path2 as HasAfter).setAfter(collect)
            val end = combineTwo(next1, next2, splitDepth + 1)
            collect.setAfter(end)
            (end as HasBefore).setBefore(collect)
            return split
        } else if (process is SplittingGateways && process2 is HasAfter) {
            combineSplitPath(process, process2, splitDepth)
        } else if (process2 is SplittingGateways && process is HasAfter) {
            combineSplitPath(process2, process, splitDepth)
        } else {
            throw NotImplementedError("Der Fall kann laut definition nicht vorkommen")
        }
    }
}

fun combineSplitPath(split: SplittingGateways, path: BPMNElement, splitDepth: Int): SplittingGateways {
    val combined = addNewPath(split, path)
    val collecting = getCollectingElement(combined)
    var next = path
    for (i in 0 until getNumBetween(combined)) {
        next = (next as HasAfter).getAfter()!!
    }
    val afterCollecting = collecting.getAfter()!!
    val end = combineTwo(afterCollecting, next, splitDepth)
    collecting.setAfter(end)
    (end as HasBefore).setBefore(collecting)
    return combined
}

fun addNewPath(split: SplittingGateways, path: BPMNElement): SplittingGateways {
    val contains = split.after.filter {
        var existing = it
        var new = path
        while (existing == new && new !is CollectingGateway) {
            existing = (existing as HasAfter).getAfter()!!
            new = (new as HasAfter).getAfter()!!
        }
        return@filter (existing is CollectingGateway)
    }.isEmpty()
    if (contains) {
        var newPath = path.toProcessElement()
        split.after.add(newPath)
        (newPath as HasBefore).setBefore(split)
        var tillConnecting = (split.after.first() as HasAfter).getAfter()!!
        while (tillConnecting !is CollectingGateway) {
            val next = tillConnecting.toProcessElement()
            (next as HasBefore).setBefore(newPath)
            (newPath as HasAfter).setAfter(next)
            newPath = next
            tillConnecting = (tillConnecting as HasAfter).getAfter()!!
        }
        tillConnecting.before.add(newPath)
        (newPath as HasAfter).setAfter(tillConnecting)
    }
    return split
}

fun copyToProcess(start: BPMNElement): BPMNElement {
    val next: BPMNElement
    val first: BPMNElement
    val last: HasAfter
    when (start) {
        is SplittingGateways -> {
            first = start.toProcessElement() as SplittingGateways
            last = copySplittingToCollecting(first, start)
            next = getCollectingElement(start).getAfter()!!
        }
        is EndEvent -> {
            return start.toProcessElement()
        }
        else -> {
            next = (start as HasAfter).getAfter()!!
            first = start.toProcessElement()
            last = first as HasAfter
        }
    }
    val copied = copyToProcess(next)
    last.setAfter(copied)
    (copied as HasBefore).setBefore(last as BPMNElement)
    return first
}

fun copySplittingToCollecting(startGateway: SplittingGateways, elem: SplittingGateways): CollectingGateway {
    val collectingGateway = getCollectingElement(elem).toProcessElement() as CollectingGateway
    elem.after.forEach {
        val startElement = it.toProcessElement()
        val copied = if (startElement is SplittingGateways) {
            copySplittingToCollecting(startElement, it as SplittingGateways)
        } else {
            copyToCollecting(startElement, it)
        }
        collectingGateway.before.add(copied)
        (copied as HasAfter).setAfter(collectingGateway)

        startGateway.after.add(startElement)
        (startElement as HasBefore).setBefore(startGateway)
        getConfig()
    }
    return collectingGateway
}

fun copyToCollecting(start: BPMNElement, elem: BPMNElement): BPMNElement {
    val copy = if (start == elem.toProcessElement()) {
        start
    } else {
        elem.toProcessElement()
    }
    val copied = if (elem is SplittingGateways) {
        copySplittingToCollecting(copy as SplittingGateways, elem)
    } else {
        val after = (elem as HasAfter).getAfter()!!
        if (after is CollectingGateway) {
            copy
        } else {
            copyToCollecting(copy, after)
        }
    }
    if (start != copy) {
        (start as HasAfter).setAfter(copy)
        (copy as HasBefore).setBefore(start)
    }
    return copied
}

fun getCollectingElement(start: SplittingGateways): CollectingGateway {
    return start.after.map {
        var cur = it
        while (cur !is CollectingGateway) {
            cur = if (cur is SplittingGateways) {
                val collecting = getCollectingElement(cur)
                collecting.getAfter()!!
            } else {
                (cur as HasAfter).getAfter()!!
            }
        }
        return@map cur
    }.toHashSet().first() as CollectingGateway
}

fun getSplittingElement(end: CollectingGateway): SplittingGateways {
    return end.before.map {
        var cur = it
        while (cur !is SplittingGateways) {
            cur = if (cur is CollectingGateway) {
                val collecting = getSplittingElement(cur)
                collecting.getBefore()!!
            } else {
                (cur as HasBefore).getBefore()!!
            }
        }
        return@map cur
    }.toHashSet().first() as SplittingGateways
}

fun getNumBetween(start: SplittingGateways): Int {
    var count = 0
    var cur = start.after.first()
    while (cur !is CollectingGateway) {
        cur = (cur as HasAfter).getAfter()!!
        count++
    }
    return count
}

fun inDBProcess(process: BPMNElement, fromDB: BPMNElement): Boolean {
    if (process == fromDB) {
        if (process is HasBefore && fromDB is HasBefore) {
            return inDBProcess(process.getBefore()!!, fromDB.getBefore()!!)
        } else if (process is CollectingGateway && fromDB is CollectingGateway) {
            if (process.before.size != fromDB.before.size) return false
            process.before.forEach { path1 ->
                fromDB.before.firstOrNull { path2 ->
                    inDBProcess(path1, path2)
                } ?: return false
            }
            return true
        } else if (process is StartedEvent) {
            return true
        }
    }
    if (fromDB is CollectingGateway && process is HasBefore) {
        return fromDB.before.any { path ->
            inDBProcess(process, path)
        }
    }
    if (fromDB is SplittingGateways && process is HasBefore) {
        return inDBProcess(process, fromDB.getBefore()!!)
    }
    return false
}
