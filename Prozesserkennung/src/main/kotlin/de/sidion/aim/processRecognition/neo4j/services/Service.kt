package de.sidion.aim.processRecognition.neo4j.services

import de.sidion.aim.processRecognition.neo4j.Entity
import de.sidion.aim.processRecognition.neo4j.Neo4JSessionFactory

interface Service<T : Entity> {
    fun findAll(): Iterable<T>
    fun findAllByProcessName(): Iterable<T>
    fun find(id: Long): T
    fun delete(id: Long)
    fun delete(element: T)
    fun save(element: T): T
}