package de.sidion.aim.processRecognition.neo4j

import org.neo4j.ogm.annotation.GeneratedValue
import org.neo4j.ogm.annotation.Id

abstract class Entity {
    @Id
    @GeneratedValue
    private lateinit var id: Number
    fun getId(): Long {
        return try {
            id.toLong()
        } catch (ex: UninitializedPropertyAccessException) {
            -1
        }
    }

    fun setId(value: Long) {
        if (value != -1L) {
            id = value
        }
    }
}