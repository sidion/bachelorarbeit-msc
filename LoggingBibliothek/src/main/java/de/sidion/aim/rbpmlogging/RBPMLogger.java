package de.sidion.aim.rbpmlogging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class RBPMLogger {
    private static final Logger logger = LoggerFactory.getLogger("RBPM-Logger");
    private static final Marker marker = MarkerFactory.getMarker("RBPM-Logger");
    private static final ObjectMapper mapper = new ObjectMapper();

    private final String instanceId, processName;

    public RBPMLogger(String processName, String instanceId) {
        this.instanceId = instanceId;
        this.processName = processName;
    }

    public void log(ProcessState state) {
        log(state, "");
    }

    public void log(ProcessState state, String stepName) {
        log(state, stepName, new HashMap<>());
    }

    public void log(ProcessState state, String stepName, Map<String, String> businessObjects) {
        if (state == null) {
            throw new IllegalArgumentException("state could not be null");
        }
        if (state.isWithDescription() && (stepName == null || stepName.isEmpty())) {
            throw new IllegalArgumentException(state.name() + " could not be logged without stepName");
        }
        try {
            logger.info(marker, mapper.writeValueAsString(
                    new LogEntry(instanceId, processName, state, stepName, businessObjects)
            ));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    private static class LogEntry {
        public String instanceId, stepName, processName, timestamp;
        public ProcessState state;
        private Map<String, String> businessObjects;

        public LogEntry(String instanceId, String processName, ProcessState state, String stepName,
                        Map<String, String> businessObjects) {
            this.instanceId = instanceId;
            this.stepName = stepName;
            this.processName = processName;
            this.state = state;
            this.businessObjects = businessObjects;
            this.timestamp = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);
        }

        public String getBusinessObjects() {
            return businessObjects.entrySet().stream().map(entry -> {
                return entry.getKey() + ":" + entry.getValue() + ";";
            }).collect(Collectors.joining());
        }
    }
}
