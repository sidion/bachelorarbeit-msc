package de.sidion.aim.rbpmlogging;

public enum ProcessState {
    STARTED(false), WORK_IN_PROGRESS(true),
    EXCEPTION_OCCURRED(true), SUCCESSFULLY_DONE(false), UNSUCCESSFULLY_DONE(false);
    private final boolean withDescription;

    ProcessState(boolean withDescription) {
        this.withDescription = withDescription;
    }

    boolean isWithOutDescription() {
        return !withDescription;
    }

    boolean isWithDescription() {
        return withDescription;
    }
}