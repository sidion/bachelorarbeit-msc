package de.sidion.aim.processRecognition.darstellung

data class Config(
    val kafka: Kafka,
    val modelDirectory: String
) {
    data class Kafka(
        val bootstrapServers: String,
        val schemaRegistryUrl: String,
        val topics: Topics
    ){
        data class Topics (
            val forProcess: String
        )
    }
}