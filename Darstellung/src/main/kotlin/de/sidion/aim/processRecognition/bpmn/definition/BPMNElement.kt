package de.sidion.aim.processRecognition.bpmn.definition

interface HasAfter {
    fun getAfter(): BPMNElement
    fun setAfter(after: BPMNElement)
}

interface HasBefore {
    fun getBefore(): BPMNElement
    fun setBefore(before: BPMNElement)
}

abstract class BPMNElement(private val isInstance: Boolean) {
    var processName = ""
    var instanceId = ""
    var stepName = ""
    var timestamp = ""

    fun isInstance() = isInstance

    abstract fun getStartEvent(): StartedEvent
    abstract fun getEndEvent(): EndEvent
    protected abstract fun getAsProcessElement(): BPMNElement

    fun toProcessElement(): BPMNElement {
        val elem = getAsProcessElement()
        elem.processName = this.processName
        elem.stepName = this.stepName
        return elem
    }

    override fun toString(): String {
        return "${this::class.simpleName}(isInstance=$isInstance, processName='$processName', instanceId='$instanceId', " +
                "stepName='$stepName', timestamp='$timestamp')"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BPMNElement

        if (processName != other.processName) return false
        if (instanceId != other.instanceId) return false
        if (stepName != other.stepName) return false
        if (timestamp != other.timestamp) return false

        return true
    }

    override fun hashCode(): Int {
        var result = processName.hashCode()
        result = 31 * result + instanceId.hashCode()
        result = 31 * result + stepName.hashCode()
        result = 31 * result + timestamp.hashCode()
        return result
    }
}

abstract class Event(isInstance: Boolean) : BPMNElement(isInstance)

abstract class Activities(isInstance: Boolean) : BPMNElement(isInstance), HasBefore, HasAfter {
    private var before: BPMNElement? = null
    private var after: BPMNElement? = null

    override fun getAfter() = after!!
    override fun setAfter(after: BPMNElement) {
        if (after == this) {
            throw IllegalArgumentException("You could not be your own after step")
        }
        this.after = after
    }

    override fun getBefore() = before!!
    override fun setBefore(before: BPMNElement) {
        if (before == this) {
            throw IllegalArgumentException("You could not be your own before step")
        }
        this.before = before
    }
}

abstract class Gateways(isInstance: Boolean) : BPMNElement(isInstance)
