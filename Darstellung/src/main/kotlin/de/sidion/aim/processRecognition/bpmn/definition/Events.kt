package de.sidion.aim.processRecognition.bpmn.definition

class StartedEvent : BPMNElement, HasAfter {
    constructor(): super(true)
    constructor(isInstance: Boolean): super(isInstance)

    private var after: BPMNElement? = null

    override fun getStartEvent() = this
    override fun getEndEvent() = after!!.getEndEvent()
    override fun getAfter() = after!!
    override fun setAfter(after: BPMNElement) {
        if (after == this) {
            throw IllegalArgumentException("You could not be your own after step")
        }
        this.after = after
    }
    override fun getAsProcessElement() = StartedEvent(false)
}

abstract class EndEvent(isInstance: Boolean) : Event(isInstance), HasBefore {
    private var before: BPMNElement? = null
    override fun getBefore() = before!!

    override fun setBefore(before: BPMNElement) {
        if (before == this) {
            throw IllegalArgumentException("You could not be your own before step")
        }
        this.before = before
    }

    override fun getStartEvent() = before!!.getStartEvent()
    override fun getEndEvent() = this
}

class SuccessfulEndEvent : EndEvent {
    constructor(): super(true)
    constructor(isInstance: Boolean): super(isInstance)
    override fun getAsProcessElement() = SuccessfulEndEvent(false)
}

class UnsuccessfulEndEvent : EndEvent {
    constructor(): super(true)
    constructor(isInstance: Boolean): super(isInstance)
    override fun getAsProcessElement() = UnsuccessfulEndEvent(false)
}

class ExceptionEndEvent: EndEvent {
    constructor(): super(true)
    constructor(isInstance: Boolean): super(isInstance)
    override fun getAsProcessElement() = ExceptionEndEvent(false)
}
