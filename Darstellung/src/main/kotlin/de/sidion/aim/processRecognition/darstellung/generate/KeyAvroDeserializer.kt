package de.sidion.aim.processRecognition.darstellung.generate

import de.sidion.aim.EventKey
import de.sidion.aim.processRecognition.darstellung.getLogger
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient
import io.confluent.kafka.serializers.AbstractKafkaAvroDeserializer
import org.apache.kafka.common.serialization.Deserializer
import java.nio.charset.Charset

class KeyAvroDeserializer(): AbstractKafkaAvroDeserializer(), Deserializer<EventKey> {
    val logger = getLogger(KeyAvroDeserializer::class)
    constructor(client: SchemaRegistryClient?): this() {
        schemaRegistry = client
    }

    constructor(client: SchemaRegistryClient?, props: Map<String?, *>?): this() {
        schemaRegistry = client
        configure(this.deserializerConfig(props))
    }

    override fun deserialize(topic: String, data: ByteArray): EventKey {
        logger.info(data.toString(Charset.defaultCharset()))
        val deserialized = deserialize(data)
        logger.info(deserialized.toString())
        return deserialize(data) as EventKey
    }
}