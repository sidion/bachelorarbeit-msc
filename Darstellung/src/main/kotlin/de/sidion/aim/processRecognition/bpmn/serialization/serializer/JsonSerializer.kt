package de.sidion.aim.processRecognition.bpmn.serialization.serializer

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.kafka.common.errors.SerializationException
import org.apache.kafka.common.serialization.Serializer


abstract class JsonSerializer<T> : Serializer<T> {
    protected val mapper = ObjectMapper()

    override fun serialize(p0: String?, p1: T): ByteArray {
        try {
            return mapper.writeValueAsBytes(p1)
        } catch (ex: Exception) {
            throw SerializationException("Error Serializing Object", ex)
        }
    }
}
