package de.sidion.aim.processRecognition.darstellung.generate

import de.sidion.aim.processRecognition.bpmn.serialization.serializer.EndEventDeserializer
import de.sidion.aim.processRecognition.darstellung.getConfig
import io.confluent.kafka.serializers.KafkaAvroDeserializer
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.serialization.Deserializer
import java.util.*

abstract class AbstractConsumer<K, V>(
    val topic: String,
    group: String,
    keyDeserializer: Class<out Deserializer<*>>,
    valueDeserializer: Class<out Deserializer<*>>
) : Runnable {
    private val properties = Properties()
    protected val consumer: KafkaConsumer<K, V>

    init {
        val config = getConfig()
        properties[ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG] = config.kafka.bootstrapServers
        properties[ConsumerConfig.GROUP_ID_CONFIG] = group
        properties[ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG] = keyDeserializer
        properties[ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG] = valueDeserializer
        properties[KafkaAvroDeserializerConfig.SCHEMA_REGISTRY_URL_CONFIG] = config.kafka.schemaRegistryUrl
        properties[KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG] = true
        consumer = KafkaConsumer(properties)
    }
}