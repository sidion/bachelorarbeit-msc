package de.sidion.aim.processRecognition.bpmn.serialization

import com.fasterxml.jackson.core.type.TypeReference
import de.sidion.aim.processRecognition.bpmn.definition.BPMNElement
import de.sidion.aim.processRecognition.bpmn.serialization.serializer.BPMNElementJsonDeserializer
import de.sidion.aim.processRecognition.bpmn.serialization.serializer.BPMNElementJsonSerializer
import org.apache.kafka.common.serialization.Deserializer
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.common.serialization.Serializer


class BPMNElementSerde : Serde<BPMNElement> {
    private val serde = Serdes.serdeFrom(
        BPMNElementJsonSerializer(
            BPMNElement::class.java
        ),
        object : BPMNElementJsonDeserializer<BPMNElement>(BPMNElement::class.java) {
            override fun getEntityType(): TypeReference<BPMNElement> {
                return object : TypeReference<BPMNElement>() {}
            }
        })

    override fun deserializer(): Deserializer<BPMNElement> {
        return serde.deserializer()
    }

    override fun serializer(): Serializer<BPMNElement> {
        return serde.serializer()
    }
}
