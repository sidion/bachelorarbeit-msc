package de.sidion.aim.processRecognition.bpmn.serialization

import com.fasterxml.jackson.core.type.TypeReference
import de.sidion.aim.processRecognition.bpmn.definition.EndEvent
import de.sidion.aim.processRecognition.bpmn.serialization.serializer.BPMNElementJsonDeserializer
import de.sidion.aim.processRecognition.bpmn.serialization.serializer.BPMNElementJsonSerializer
import org.apache.kafka.common.serialization.Deserializer
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.common.serialization.Serializer

class ProcessSerde : Serde<EndEvent> {

    private val serde = Serdes.serdeFrom(
        BPMNElementJsonSerializer(
            EndEvent::class.java
        ),
        object : BPMNElementJsonDeserializer<EndEvent>(EndEvent::class.java) {
            override fun getEntityType(): TypeReference<EndEvent> {
                return object : TypeReference<EndEvent>() {}
            }
        })

    override fun deserializer(): Deserializer<EndEvent> {
        return serde.deserializer()
    }

    override fun serializer(): Serializer<EndEvent> {
        return serde.serializer()
    }
}