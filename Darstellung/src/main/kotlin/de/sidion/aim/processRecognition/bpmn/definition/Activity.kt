package de.sidion.aim.processRecognition.bpmn.definition

class Activity : Activities {
    constructor(): super(true)
    constructor(isInstance: Boolean): super(isInstance)

    override fun getStartEvent() = getBefore().getStartEvent()
    override fun getEndEvent() = getAfter().getEndEvent()
    override fun getAsProcessElement() = Activity(false)
}