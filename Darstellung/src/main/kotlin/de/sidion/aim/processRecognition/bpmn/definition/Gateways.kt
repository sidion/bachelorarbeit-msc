package de.sidion.aim.processRecognition.bpmn.definition

abstract class SplittingGateways(isInstance: Boolean) : Gateways(isInstance), HasBefore {
    private var before: BPMNElement? = null
    val after = mutableSetOf<BPMNElement>()

    override fun getStartEvent() = before!!.getStartEvent()
    override fun getEndEvent() = after.first().getEndEvent()
    override fun getBefore() = before!!

    override fun setBefore(before: BPMNElement) {
        if (before == this) {
            throw IllegalArgumentException("You could not be your own before step")
        }
        this.before = before
    }
}

class ExclusiveGateway : SplittingGateways {
    constructor(): super(true)
    constructor(isInstance: Boolean): super(isInstance)
    override fun getAsProcessElement() = ExclusiveGateway(false)
}

class ParallelGateway : SplittingGateways {
    constructor(): super(true)
    constructor(isInstance: Boolean): super(isInstance)
    override fun getAsProcessElement() = ParallelGateway(false)
}

class CollectingGateway : Gateways, HasAfter {
    constructor(): super(true)
    constructor(isInstance: Boolean): super(isInstance)
    val before = mutableSetOf<BPMNElement>()
    private var after: BPMNElement? = null

    override fun getAfter() = after!!
    override fun setAfter(after: BPMNElement) {
        if (after == this) {
            throw IllegalArgumentException("You could not be your own after step")
        }
        this.after = after
    }
    override fun getStartEvent() = before.first().getStartEvent()
    override fun getEndEvent() = after!!.getEndEvent()
    override fun getAsProcessElement() = CollectingGateway(false)
}
