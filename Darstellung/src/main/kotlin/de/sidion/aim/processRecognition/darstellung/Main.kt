package de.sidion.aim.processRecognition.darstellung

import de.sidion.aim.processRecognition.darstellung.generate.ModelConsumer
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.reflect.KClass
import kotlin.system.exitProcess

open class Main {
    companion object {
        @JvmStatic
        var config: Config? = null
    }
}

fun main(vararg args: String) {
    createConfig()
    val test = ModelConsumer()
    test.run()

    Runtime.getRuntime().addShutdownHook(object : Thread("streams shutdown hook") {
        override fun run() {
            test.shutdown()
        }
    })
}

fun createConfig() {
    val logger = getLogger(Main::class)
    if (Main.config == null) {
        val config = Config(
            Config.Kafka(
                System.getenv("KAFKA_BOOTSTRAP_SERVERS") ?: "",
                System.getenv("SCHEMA_REGISTRY_URL") ?: "",
                Config.Kafka.Topics(
                    System.getenv("KAFKA_TOPIC_FOR_PROCESS") ?: ""
                )
            ),
            System.getenv("BPMN_MODEL_DIRECTORY") ?: ""
        )
        Main.config = config
        if (config.kafka.bootstrapServers == "") {
            logger.error("Environment variable KAFKA_BOOTSTRAP_SERVERS has to be set")
            Main.config = null
        }
        if (config.kafka.schemaRegistryUrl == "") {
            logger.error("Environment variable SCHEMA_REGISTRY_URL has to be set")
            Main.config = null
        }
        if (config.kafka.topics.forProcess == "") {
            logger.error("Environment variable KAFKA_TOPIC_FOR_PROCESS has to be set")
            Main.config = null
        }
        if (config.modelDirectory == "") {
            logger.error("Environment variable BPMN_MODEL_DIRECTORY has to be set")
            Main.config = null
        }

        if (Main.config == null) {
            exitProcess(1)
        }
    }
}

fun getConfig(): Config {
    return Main.config ?: throw NullPointerException()
}

fun getLogger(forClass: Class<*>): Logger = LoggerFactory.getLogger(forClass)
fun getLogger(forClass: KClass<*>): Logger = LoggerFactory.getLogger(forClass.java)

fun String.toVaildId(): String {
    return this.replace("ä", "ae", true)
        .replace("ö", "oe", true)
        .replace("ü", "ue", true)
        .replace("ß", "ss", true)
        .split(" ").joinToString("") { it.capitalize() }
}
