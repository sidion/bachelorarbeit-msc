package de.sidion.aim.processRecognition.darstellung.generate

import de.sidion.aim.EventKey
import de.sidion.aim.processRecognition.bpmn.definition.*
import de.sidion.aim.processRecognition.bpmn.serialization.serializer.EndEventDeserializer
import de.sidion.aim.processRecognition.darstellung.getConfig
import de.sidion.aim.processRecognition.darstellung.getLogger
import de.sidion.aim.processRecognition.darstellung.toVaildId
import io.confluent.kafka.serializers.KafkaAvroDeserializer
import org.camunda.bpm.model.bpmn.Bpmn
import org.camunda.bpm.model.bpmn.BpmnModelInstance
import org.camunda.bpm.model.bpmn.instance.Definitions
import org.camunda.bpm.model.bpmn.instance.FlowNode
import org.camunda.bpm.model.bpmn.instance.Process
import org.camunda.bpm.model.bpmn.instance.SequenceFlow
import java.io.File
import java.time.Duration
import java.util.*
import kotlin.random.Random
import kotlin.random.nextInt
import org.camunda.bpm.model.bpmn.instance.EndEvent as CamundaEndEvent
import org.camunda.bpm.model.bpmn.instance.ExclusiveGateway as CamundaExclusiveGateway
import org.camunda.bpm.model.bpmn.instance.Gateway as CamundaGateway
import org.camunda.bpm.model.bpmn.instance.ParallelGateway as CamundaParallelGateway
import org.camunda.bpm.model.bpmn.instance.StartEvent as CamundaStartEvent
import org.camunda.bpm.model.bpmn.instance.Task as CamundaTask

class ModelConsumer :
    AbstractConsumer<EventKey, EndEvent>(
        getConfig().kafka.topics.forProcess,
        "create-bpmn-model",
        KafkaAvroDeserializer::class.java,
        EndEventDeserializer::class.java
    ) {
    private val logger = getLogger(ModelConsumer::class)

    override fun run() {
        consumer.use { consumer ->
            consumer.subscribe(listOf(topic))
            while (true) {
                val records = consumer.poll(Duration.ofDays(7))
                for (record in records) {
                    val bpmnModel = toBpmnModel(record.value())
                    Bpmn.validateModel(bpmnModel)
                    val file = File.createTempFile("model-api-", ".bpm")
                    Bpmn.writeModelToFile(file, bpmnModel)
                    logger.info("Transformed ${record.value().processName}")

                    val layouted = BPMNDiagramGenerator.generateLayout(file)
                    logger.info("Created Layout ${record.value().processName}")
                    val filename = if (getConfig().modelDirectory.last() == File.separatorChar)
                        getConfig().modelDirectory + record.value().processName.toVaildId() + ".bpmn"
                    else getConfig().modelDirectory + File.separatorChar + record.value().processName.toVaildId() + ".bpmn"
                    val readyFile = File(filename)
                    if (readyFile.exists()) {
                        readyFile.delete()
                    }
                    readyFile.createNewFile()
                    Bpmn.writeModelToFile(readyFile, layouted)
                }
            }
        }
    }

    fun shutdown() {
        consumer.wakeup()
    }

    private fun toBpmnModel(element: EndEvent): BpmnModelInstance {
        return BpmnModelCreator(
            Bpmn.createEmptyModel(),
            element.processName.toVaildId()
        ).createProcess(element.getStartEvent())
    }

    private class BpmnModelCreator(val modelInstance: BpmnModelInstance, processName: String) {
        val definitions: Definitions
        val process: Process

        init {
            definitions = modelInstance.newInstance(Definitions::class.java)
            definitions.name = processName.toVaildId()
            definitions.targetNamespace = "de.sidion.aim"
            modelInstance.definitions = definitions

            process = modelInstance.newInstance(Process::class.java, processName)
            definitions.addChildElement(process)
        }

        fun createProcess(startedEvent: StartedEvent): BpmnModelInstance {
            toProcess(startedEvent, Stack(), -1)
            return modelInstance
        }

        private fun toProcess(element: BPMNElement, lastSplit: Stack<CamundaGateway>, index: Int): FlowNode {
            val step = if (element is CollectingGateway) {
                if (index != 0) {
                    return lastSplit.pop()
                }
                lastSplit.pop()
            } else {
                val idAppendix = if (index >= 1) "$index" else ""
                createElement(element, idAppendix)
            }

            if (element is HasAfter) {
                val next = toProcess(element.getAfter(), lastSplit, index)
                createFlow(step, next)
            } else if (element is SplittingGateways) {
                val newLastSplit = createElement(element, "closing")
                element.after.mapIndexed { innerIndex, innerElement ->
                    lastSplit.push(newLastSplit as CamundaGateway)
                    val next = toProcess(innerElement, lastSplit, innerIndex)
                    createFlow(step, next)
                }
            }
            return step
        }

        private fun createElement(
            fromBPMNElement: BPMNElement,
            idAppendix: String = ""
        ): FlowNode {
            val idFirst = if (fromBPMNElement.stepName.isBlank()) {
                "random-${Random.nextInt(100000..999999)}"
            } else {
                fromBPMNElement.stepName.toVaildId()
            }
            val id = if (idAppendix.isEmpty()) idFirst else "$idFirst-$idAppendix"
            val newElement = when (fromBPMNElement) {
                is StartedEvent -> modelInstance.newInstance(CamundaStartEvent::class.java, id)
                is SuccessfulEndEvent -> modelInstance.newInstance(CamundaEndEvent::class.java, id)
                is UnsuccessfulEndEvent -> modelInstance.newInstance(CamundaEndEvent::class.java, id)
                is ExceptionEndEvent -> modelInstance.newInstance(CamundaEndEvent::class.java, id)
                is Activity -> {
                    val cur = modelInstance.newInstance(CamundaTask::class.java, id)
                    cur.name = fromBPMNElement.stepName
                    cur
                }
                is ExclusiveGateway -> modelInstance.newInstance(CamundaExclusiveGateway::class.java, id)
                is ParallelGateway -> modelInstance.newInstance(CamundaParallelGateway::class.java, id)
                is CollectingGateway -> throw IllegalArgumentException("Has to be specified by Opening type")
                else -> throw IllegalArgumentException("Path is not possible")
            }
            process.addChildElement(newElement)
            return newElement
        }

        private fun createFlow(from: FlowNode, to: FlowNode): SequenceFlow {
            val flow = modelInstance.newInstance(SequenceFlow::class.java, "${from.id}-${to.id}")
            process.addChildElement(flow)
            flow.source = from
            from.outgoing.add(flow)
            flow.target = to
            to.incoming.add(flow)
            return flow
        }
    }
}