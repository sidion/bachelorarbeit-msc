package de.sidion.aim.processRecognition.bpmn.serialization.serializer

import com.fasterxml.jackson.core.type.TypeReference
import de.sidion.aim.processRecognition.bpmn.definition.EndEvent

class EndEventDeserializer : BPMNElementJsonDeserializer<EndEvent>(EndEvent::class.java) {
    override fun getEntityType(): TypeReference<EndEvent> {
        return object : TypeReference<EndEvent>() {}
    }
}