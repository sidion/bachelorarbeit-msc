# Quellcode zur Bachlorarbeit von Michael Schweiker

In diesem Repository ist der komplette Quellcode zur Bachlorarbeit von Michael Schweiker abgelegt.
Die einzelnen Ordner enthalten den Code zu den gleich benannten Kapiteln der Arbeit.

Zum ausführen der Anwendungen wird Docker (+ Docker Compose), Maven und Gralde benötigt.
Die minimale Java-Version die zum bauen benötigt wird ist Java 11.
Da einige arbeitsspeicherintensiven Anwendungen verwendet werden, sollte Docker mindestens 8 GB Arbeitsspeicher zur Verfügung haben.

Mittles `fullstart.bat` werden alle Anwendungsteile gebaut und alle Container gestartet.

## Apache Kafka Docker
In diesem Ordner liegt eine Docker Compose Datei, mit welcher Apache Kafka gestartet werden kann.
Dazu muss in diesem Ordner nur `docker-compose up -d` ausgeführt werden.
Anschließend kann mit `docker ps` geprüft werden, ob alle Container erfolgreich gestartet sind.
Es müssen drei Container sein, bei welchen das Image mit "confluentinc/" beginnt.

## Logging-Bibliothek
Die Logging-Bibliothek wird benötigt, um die Beispielanwendung zu bauen und auszuführen.
Daswegen muss diese mittels `mvn install` gebaut und im lokalen Maven-Repository verfügbar gemacht werden.

## Kafka Connect
In diesem Ordner liegen zum einen die benötigten Java-Bibliotheken sowie der selbstgeschriebene SMT.
Dieser wird automatisch beim starten der Beispielanwendung gebaut und mit gestartet.

## Beispielanwendung
Um die Beispielanwendungen zu starten kann unter Windows die Datei `restart.bat` ausgeführt werden.
Diese baut automatisch den Java-Teil der Anwendungen und startet anschließend die benötigten Dockercontainer.
Unter [http://localhost:8083/swagger-ui.html](http://localhost:8083/swagger-ui.html) ist eine Swagger-Oberfläche, überwelche
Anfragen abgesetzt werden könnnen, zu welchen die Prozesse generiert werden.

## Prozesserkennung
Auch in diesem Ordner liegt eine `restart.bat`.
Diese baut automatisch die Anwendung und startet Neo4J.
Auf die Oberfläche von Neo4J kann nach dem starten über [http://localhost:7474](http://localhost:7474) zugegriffen werden.
Nutzername: Neo4J
Password: Neo4J2
