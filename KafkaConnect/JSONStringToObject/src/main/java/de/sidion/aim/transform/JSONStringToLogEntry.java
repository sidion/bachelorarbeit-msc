package de.sidion.aim.transform;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.sidion.aim.LogEntry;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.connect.connector.ConnectRecord;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.data.SchemaBuilder;
import org.apache.kafka.connect.data.Struct;
import org.apache.kafka.connect.transforms.Transformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class JSONStringToLogEntry<T extends ConnectRecord<T>> implements Transformation<T> {
    private static final Logger logger = LoggerFactory.getLogger(JSONStringToLogEntry.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public T apply(T record) {
        Schema schema = SchemaBuilder.struct()
                .name("de.sidion.aim.transform.LogEntry")
                .field("instanceId", Schema.STRING_SCHEMA)
                .field("stepName", Schema.STRING_SCHEMA)
                .field("processName", Schema.STRING_SCHEMA)
                .field("timestamp", Schema.STRING_SCHEMA)
                .field("state", Schema.STRING_SCHEMA)
                .field("businessObjects", Schema.STRING_SCHEMA);
        logger.debug(record.value().toString());
        Struct struct;
        try {
            LogEntry entry = mapper.readValue(record.value().toString(), LogEntry.class);
            struct = new Struct(schema)
                    .put("instanceId", entry.getInstanceId())
                    .put("stepName", entry.getStepName())
                    .put("processName", entry.getProcessName())
                    .put("timestamp", entry.getTimestamp())
                    .put("state", entry.getState())
                    .put("businessObjects", mapper.writeValueAsString(entry.getBusinessObjects()));
        } catch (JsonProcessingException e) {
            logger.error("Incompatible Message", e);
            return record;
        }
        return record.newRecord(record.topic(), record.kafkaPartition(), record.keySchema(), record.key(), record.valueSchema(),
                struct, record.timestamp());
    }

    @Override
    public ConfigDef config() {
        return new ConfigDef();
    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> configs) {

    }
}